#!/usr/bin/python

import sys
import requests
import datetime

# Settings for the domoticz server, change these before running

domoticzserver= "ip_address:port"
domoticzusername = "user"
domoticzpassword = "pass"
switchid = "idx"

# in case you want to control a dimmer light like I do
domoticzurl = 'http://'+domoticzserver+'/json.htm?type=command&param=switchlight&idx=' + switchid + '&switchcmd=Set%20Level&level=100'

# in case you simply want to turn on a light (uncomment this one by removing the # sign and place one in front of the line above)
#domoticzurl = 'http://'+domoticzserver+'/json.htm?type=command&param=switchlight&idx=99&switchcmd=Off

# DO NOT CHANGE BEYOND THIS LINE (unless you want to do something else ;)

now = datetime.datetime.now().strftime('%H:%M')
sunsetUrl = 'http://'+domoticzserver+'/json.htm?type=command&param=getSunRiseSet'

# Function that returns current sunset and sunrise times from Domoticz
def get_sunriseset():
        r = requests.get(sunsetUrl, auth=(domoticzusername, domoticzpassword))
        json_object = r.json()
        if json_object["status"] == "OK":
                sunset = json_object["Sunset"]
                sunset = datetime.datetime.strptime(sunset, '%H:%M')
                sunset = datetime.datetime.strftime(sunset, '%H:%M')
                sunrise = json_object["Sunrise"]
                sunrise = datetime.datetime.strptime(sunrise, '%H:%M')
                sunrise = datetime.datetime.strftime(sunrise, '%H:%M')
        else:
                print(json_object)
                print("Something went wrong in the json call")
                sys.exit(0)
        return sunset, sunrise

sunriseset = get_sunriseset()
sunset = sunriseset[0]
sunrise = sunriseset[1]

if now < sunset and sunrise > now or now > sunset:
        print("Still too dark to go without lights!")
        requests.put(domoticzurl, auth=(domoticzusername, domoticzpassword))
else:
        print(now + " - " + sunset + " - " + sunrise)
        print("It's not dark outside so doing nothing")

sys.exit(0)