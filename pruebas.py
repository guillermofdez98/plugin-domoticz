# include standard modules
import getopt, sys

# read commandline arguments, first
fullCmdArguments = sys.argv

# - further arguments
argumentList = fullCmdArguments[1:]

unixOptions = "h:t:"
gnuOptions = ["humidity=", "temperature="]

try:
    arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
    # output error, and return with an error code
    print (str(err))
    sys.exit(2)

temp = 0
humidity = 0

# evaluate given options
for currentArgument, currentValue in arguments:
    if currentArgument in ("-t", "--temperature"):
        print (("Set temperature at %s ºC") % (currentValue))
        temp = currentValue
    elif currentArgument in ("-h", "--humidity"):
        print (("Set humidity at %s per cent") % (currentValue))
        humidity = currentValue