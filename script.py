#!/usr/bin/python

# Librerías necesarias
import requests
import datetime
import time
import getopt, sys

# Selecciona el periodo de refresco
PERIODO = 5

# Argumentos de entrada del comando
fullCmdArguments = sys.argv
argumentList = fullCmdArguments[1:]
unixOptions = "h:t:v:"
gnuOptions = ["humidity=", "temperature=","valvula="]
try:
    arguments, values = getopt.getopt(argumentList, unixOptions, gnuOptions)
except getopt.error as err:
    print (str(err))
    sys.exit(2)

# Variables para limites
tempLimit = 0
humidityLimit = 0

# Estado de la valvula pasado por argumentos
valvulaStatus = 0

# Evaluación de los argumentos de entrada
for currentArgument, currentValue in arguments:
    if currentArgument in ("-t", "--temperature"):
        print (("Temperature set at %s ºC") % (currentValue))
        tempLimit = currentValue
    elif currentArgument in ("-h", "--humidity"):
        print (("Humidity set at %s per cent") % (currentValue))
        humidityLimit = currentValue
    elif currentArgument in ("-v", "--valvula"):
        if(currentValue == "0"):
            print("Valvula Off")
        elif(currentValue == "1"):
            print("Valvula On")
        valvulaStatus = currentValue

# Información sobre Domoticz y sensores
domoticzserver= "192.168.1.183:8080"
releId = "5"
tempHumId = "7"

# URL de la API-Rest
releStatusUrl = 'http://'+domoticzserver+'/json.htm?type=devices&rid='+releId
releOffUrl = 'http://'+domoticzserver+'/json.htm?type=command&param=switchlight&idx='+releId+'&switchcmd=Off'
releOnUrl = 'http://'+domoticzserver+'/json.htm?type=command&param=switchlight&idx='+releId+'&switchcmd=On'
tempHumUrl = 'http://'+domoticzserver+'/json.htm?type=devices&rid='+tempHumId

# Funciones necesarias

# Obtiene valor de temperatura y humedad desde Domoticz
def getTempHum():
    res = requests.get(tempHumUrl)
    json_object = res.json()
    if json_object["status"] == "OK":
        humedad = json_object["result"][0]["Humidity"]
        temperatura = json_object["result"][0]["Temp"]
    else:
        print(json_object)
        print("Something went wrong in the json call")
        sys.exit(0)
    return humedad, temperatura

# Obtiene el estado del relé desde Domoticz
def getReleStatus():
    res = requests.get(releStatusUrl)
    json_object = res.json()
    if json_object["status"] == "OK":
        estado = json_object["result"][0]["Data"]
    else:
        print(json_object)
        print("Something went wrong in the json call")
        sys.exit(0)
    return estado

# Establece el estado del relé al pasado como parametro (0: Off, 1: On)
def setReleStatus(state):
    if(state == 0):
        requests.get(releOffUrl)
    else:
        requests.get(releOnUrl)


# Bucle de ejecución
while(1):
    humedad = getTempHum()[0]
    temperatura = getTempHum()[1]

    if(humedad < int(humidityLimit) and temperatura > int(tempLimit)):
        if(getReleStatus != "On"):
            setReleStatus(1)
    else:
        if(getReleStatus != "Off"):
            setReleStatus(0)

    time.sleep(PERIODO)
